import React from "react";
import { useEffect } from "react";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import MainNoButton from "../components/MainNoButton/MainNoButton";
import Filter from "../components/Filter/Filter";

const SearchCars = () => {
  useEffect(() => {
    document.title = "Search Cars";
  }, []);

  return (
    <div>
      <Header />
      <MainNoButton />
      <Filter />
      <Footer />
    </div>
  );
};

export default SearchCars;
