import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Banner.css";

const Banner = () => {
  return (
    <div>
      <div className="container mt-3 main-banner">
        <div className="mt-4 p-5 text-white rounded text-center box">
          <h1 className="pb-4">Sewa Mobil di (Lokasimu) Sekarang</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod <br />
            tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <button className="btn mt-5">Mulai Sewa Mobil</button>
        </div>
      </div>
    </div>
  );
};

export default Banner;
