import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./OurServices.css";

const OurServices = () => {
  return (
    <div>
      {" "}
      <div className="container main-content-2" id="OurServices">
        <div className="">
          <div className="row">
            <div className="col-lg-6">
              <img
                src="./IMG/img_service.png"
                className="img-fluid"
                width="374px"
                height="366px"
                alt=""
              />
            </div>
            <div className="col-lg-6 main-contents-2">
              <h1>
                Best Car Rental for any kind of trip in <br />
                (Lokasimu)!
              </h1>
              <br />
              <p>
                Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
                lebih <br />
                murah dibandingkan yang lain, kondisi mobil baru, serta kualitas
                <br />
                pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
                meeting, dll.
              </p>
              <ul className="ps-0">
                <li>
                  <img
                    src="./IMG/Group 53.png"
                    className="img-fluid"
                    width="20px"
                    height="20px"
                    alt=""
                  />
                  Sewa Mobil Dengan Supir di Bali 12 Jam
                </li>
                <br />
                <li>
                  <img
                    src="./IMG/Group 53.png"
                    className="img-fluid"
                    width="20px"
                    height="20px"
                    alt=""
                  />
                  Sewa Mobil Lepas Kunci di Bali 24 Jam
                </li>
                <br />
                <li>
                  <img
                    src="./IMG/Group 53.png"
                    className="img-fluid"
                    width="20px"
                    height="20px"
                    alt=""
                  />
                  Sewa Mobil Jangka Panjang Bulanan
                </li>
                <br />
                <li>
                  <img
                    src="./IMG/Group 53.png"
                    className="img-fluid"
                    width="20px"
                    height="20px"
                    alt=""
                  />
                  Gratis Antar - Jemput Mobil di Bandara
                </li>
                <br />
                <li>
                  <img
                    src="./IMG/Group 53.png"
                    className="img-fluid"
                    width="20px"
                    height="20px"
                    alt=""
                  />
                  Layanan Airport Transfer / Drop In Out
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurServices;
